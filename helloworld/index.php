<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="reseter.css">
    <link rel="stylesheet" href="style.css">
    <title>Hello world!</title>
</head>

<body>
    <header>
        <div class="logo">
            <img src="logo.jpg" alt="logo">
        </div>
        <div class=" header-menu">
            Hello world!
        </div>
    </header>

    <main>
        <?php echo 'Hello world!'; ?>
    </main>

    <footer>

    </footer>
</body>

</html>